import * as React from 'react'

import { SvgIconProps } from '@material-ui/core'
import IconBlog from '@material-ui/icons/DescriptionOutlined'

export default (props: SvgIconProps) => <IconBlog {...props} fontSize='small' />
