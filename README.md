Space Engineers Praisal
================================================================================

Space Engineers Praisal (or SEPraisal, or SEP) is a website with a database behind it.

https://spaceengineerspraisal.net



Features
--------------------------------------------------------------------------------


Browse all Steam workshop blueprints!
- Filters: vanilla-ness, blocks, PCU, required ores, etc.
- Text search: narrow down by title, author, etc.
- Sort: by subscribers, blocks, PCU, and required ore.

Get in-depth analysis of the blueprint!
- 2D x-ray: Bad picture? View it in three projections.
- Costs: list all required components, ingots and ores.
- Stats: Precalculated values of most important stats.

Compare blueprints side-to-side!
- Offline blueprints: Add your offline blueprint to comparison.
- Recent: Easy access to recently viewed blueprints.
- Unlimited: Compare as many blueprints as you can.




Contributing
--------------------------------------------------------------------------------

You are welcome to contribute. Main areas to help with are:
- SE gamer: just use it and give feedback. What's broken? What features would you want? What should be done better?
- SE blueprint creator: help us understand whenever performance or quality reports make sense. Let's discuss.
- developer: essentially, to implement or improve features. See [CONTRIBUTING](./CONTRIBUTING.md).
- data scientist: essentially, to assigned class based on big data not only for fighters. See [CONTRIBUTING](./CONTRIBUTING.md).

I started this project as a hobby to pursue my curiousity about new and fancy IT technologies.
Therefore I see this project as a sandbox for various experiments and to try newest versions.
Some things may not be the best suited for its job, but they should be "good enough" and it was fun to try them out.
You're welcome to join.




License
--------------------------------------------------------------------------------

GPLv3, except for Space Engineers assets that are used with KSW permission.
